import os
import pytest
import mock


@pytest.mark.asyncio
async def test_load_uname(mock_hub, hub):
    with mock.patch.object(
        os,
        "uname",
        return_value=("Linux", "testname", "testrelease", "testversion", "testarch",),
    ):
        mock_hub.grains.uname.load_uname = hub.grains.uname.load_uname
        await mock_hub.grains.uname.load_uname()

    assert mock_hub.grains.GRAINS.kernel == "Linux"
    assert mock_hub.grains.GRAINS.nodename == "testname"
    assert mock_hub.grains.GRAINS.kernelrelease == "testrelease"
    assert mock_hub.grains.GRAINS.kernelversion == "testversion"
    assert mock_hub.grains.GRAINS.cpuarch == "testarch"
